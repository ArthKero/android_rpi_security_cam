# Projet M2 Android: Récupération d'informations depuis d'un système de surveillance

## Auteurs

+ Hervé Hugonin
+ Arthur Perroux

## Système de surveillance

Le système devra être capable de détecter une personne entrant dans le champs de vision de la caméra, puis d’envoyer une notification à la plateforme Android pour prévenir de la détection de la personne.
Optionnellement, si un moteur est disponible, la caméra devra être capable de garder la personne détectée dans le champs de vision de la caméra.
Une application Android sera fournie et devra recevoir les informations que la RPi lui communique.
Les personnes détectées seront classifiées en deux types, des personnes "connues" et "inconnues".
Les personnes connues sont autorisées à entrer et ne déclenchent donc pas de réaction du système.
Les personnes inconnues déclenchent une réaction sur le système RPi qui envoie une notification à l’application Android.

## Spécification

+ __Récupération d'images__: Récupérer des photos envoyées depuis la RPi.
+ __Récupération d'un flux vidéo__: Récupérer le flux live vidéo de la RPi.
+ __Système de notification__: Informer l'utilisateur d'une intrusion.
+ __Base de données locale__: Enregistrer des personnes détectées.
+ _(optionel) asservissement distant: Asservissement du moteur de la caméra._
