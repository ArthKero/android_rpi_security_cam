package com.example.hugon.raspberryconnection;



/**
 * Created by hugon on 17/10/2017.
 */

public class RaspNotif {
    private String url;
    private String Nom ;
    private String Date;

    public RaspNotif(){}

    public RaspNotif( String name, String date) {
        Nom = name;
        this.Date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
