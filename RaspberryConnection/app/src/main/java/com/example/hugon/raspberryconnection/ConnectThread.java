package com.example.hugon.raspberryconnection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by hugon on 24/11/2017.
 */


public class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private BluetoothDevice mmDevice;
        private Set<BluetoothDevice> devices;
        private final BluetoothAdapter mBluetoothAdapter;
        private OutputStream out ;
        private  Context context;
        private UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");

        public ConnectThread(BluetoothAdapter adapter) {
            BluetoothSocket tmp = null;
            mBluetoothAdapter=adapter;

            devices = mBluetoothAdapter.getBondedDevices();
            for (BluetoothDevice blueDevice : devices) {
                if (blueDevice.getName().equals("securitycam       ")) {
                    mmDevice = blueDevice;
                }


            }
            try {
                if(mmDevice != null)
                 tmp = mmDevice.createRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) { }
            mmSocket = tmp;
        }

        public void run() {
            mBluetoothAdapter.cancelDiscovery();
            try {
                mmSocket.connect();
                out = mmSocket.getOutputStream();
            } catch (IOException connectException) {
                try {
                    Log.d("BLUETOOTH","unable to connect");
                    Log.e("BLUETOOTH_ERROR",connectException.getMessage());
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e("ERROR_BLUETOOTH",closeException.getMessage());
                }
                return;
            }

            if(mmSocket.isConnected())
                Log.d("BLUETOOTH","connected");
        }

        public void send(String message){
            try {
                out.write(message.getBytes());

            }catch (IOException outException){}

        }
        public void cancel() {
            try {
                mmSocket.close();
                if (out != null)
                    out.close();
            } catch (IOException e) { }
        }

    }

