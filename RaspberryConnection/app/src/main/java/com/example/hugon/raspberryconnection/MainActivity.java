package com.example.hugon.raspberryconnection;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUESTS = 123;
    RecyclerView recyclerView;
    List<RaspNotif> notifs ;
    private DrawerLayout mDrawerLayout;
    private FirebaseAuth auth;
    private FirebaseDatabase database;
    protected DatabaseReference myRef ;
    private RecyclerView.Adapter adapter;
    NavigationView nvDrawer;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this,this.mDrawerLayout,0,0);
        mDrawerLayout.addDrawerListener(this.mDrawerToggle);
        nvDrawer = (NavigationView)findViewById(R.id.nView);
        auth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users");

            // demande de la permission internet
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
             if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                     Manifest.permission.INTERNET)) {


            } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET},MY_PERMISSIONS_REQUESTS);
             }
            }

        //Support du boutton retour
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ic_drawer);
            setupDrawerContent(nvDrawer);
        }

        notifs = new ArrayList<>();
        //fakeList();
        adapter= new RecyclerAdapter(notifs);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter(adapter);

    }

    private void setupDrawerContent(NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener(){
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem){
                        selectDrawerItem(menuItem);
                        return true;
                    }
                }
        );
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
    switch (requestCode) {
    case MY_PERMISSIONS_REQUESTS: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay! Do the
            // contacts-related task you need to do.

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
             }

        }

         // other 'case' lines to check for other
        // permissions this app might request
    }
    }

    public void selectDrawerItem(MenuItem menuItem){
        Intent intent;
        switch (menuItem.getItemId()){
            case R.id.log:
                intent = new Intent(this.getBaseContext(),Logs.class);
                startActivity(intent);
                break;
            case R.id.config:
                intent = new Intent(this.getBaseContext(),Configurations.class);
                startActivity(intent);
                break;
            case R.id.servo:
                intent = new Intent(this.getBaseContext(),Pilotage_servomoteur.class);
                startActivity(intent);
                break;
            case R.id.deconnexion:
                auth.signOut();
                intent = new Intent(this.getBaseContext(),Login.class);
                startActivity(intent);
                finish();
                break;

        }
        menuItem.setChecked(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ajoute les entrées de menu_test à l'ActionBar
        // getMenuInflater().inflate(R.menu.artisan_menu, menu);
        return true;
    }

    //gère le click sur une action de l'ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //ecrasement de la list actuel contre la nouvelle
                notifs.clear();
                for(DataSnapshot contactsData : dataSnapshot.getChildren()) {
                    RaspNotif newNotif = contactsData.getValue(RaspNotif.class);
                    notifs.add(newNotif);
                }

                //refresh le recycler
                adapter.notifyDataSetChanged();


            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        }

}
