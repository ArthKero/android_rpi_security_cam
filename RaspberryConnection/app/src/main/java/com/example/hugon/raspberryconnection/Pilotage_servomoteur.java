package com.example.hugon.raspberryconnection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Set;

import static com.example.hugon.raspberryconnection.Common.ADRESS_FLUX;

public class Pilotage_servomoteur extends AppCompatActivity {

    private BluetoothAdapter BAdapter;
    private ConnectThread connectThread;
    private WebView flux ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilotage_servomoteur);

        flux = (WebView) findViewById(R.id.flux_video);
        flux.setWebViewClient(new MyBrowser());

        //Support du boutton retour
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ic_drawer);
        }
        startBluetooth();
        connectToRasp();
        flux.getSettings().setLoadsImagesAutomatically(true);
        flux.getSettings().setJavaScriptEnabled(true);
        flux.getSettings().setLoadWithOverviewMode(true);
        flux.getSettings().setUseWideViewPort(true);
        flux.getSettings().setBuiltInZoomControls(true);
        // flux.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        flux.loadUrl(ADRESS_FLUX);


    }

    @Override
    protected void onStart() {
        super.onStart();



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void startBluetooth() {
        BAdapter= BluetoothAdapter.getDefaultAdapter();

    }
    public void move_servo(View v){

        switch (v.getId()){
            case (R.id.upButton):
                connectThread.send("8");
                break;
            case (R.id.downButton):
                connectThread.send("2");
                break;
            case (R.id.leftButton):
                connectThread.send("4");
                break;
            case (R.id.rightButton):
                connectThread.send("6");
                break;

        }
    }
    private void connectToRasp(){


    connectThread = new ConnectThread(BAdapter);
    connectThread.run();

}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        connectThread.cancel();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
