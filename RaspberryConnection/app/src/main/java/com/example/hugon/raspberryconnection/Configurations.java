package com.example.hugon.raspberryconnection;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Set;

/**
 * Created by hugon on 13/10/2017.
 */

public class Configurations extends AppCompatActivity {

    Button saveButton;
    Switch wifi_Bluetooth;
    BluetoothAdapter BAdapter;
    private static final int REQUEST_CODE_BLUETOOTH = 38;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuration);

        saveButton = (Button)findViewById(R.id.save);
        wifi_Bluetooth = (Switch) findViewById(R.id.switch1);

        //Support du boutton retour
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void save (View v ){

        if(wifi_Bluetooth.isChecked()){
            //bluetooth is choosed
            checkBluetooth();

        }else{
            //wifi is choosed
            checkWifi();
        }
        
        Toast.makeText(this,"sauvegardé ! ",Toast.LENGTH_SHORT).show();

    }


    private void checkBluetooth() {
        BAdapter= BluetoothAdapter.getDefaultAdapter();
        if(BAdapter != null ) {
            startBluetooth();
        }
        else{
            Log.d("Bluetooth","Bluetooth not supported");
            finish();
        }
    }

    private void startBluetooth() {

        if(!BAdapter.isEnabled()){
            Intent intent = new Intent(BAdapter.ACTION_REQUEST_ENABLE);
            startActivity(intent);
        }
    }

    private void checkWifi() {

    
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
        case REQUEST_CODE_BLUETOOTH:
            if(resultCode == RESULT_OK) {
                Toast.makeText(this, "bluetooth enabled", Toast.LENGTH_SHORT).show();
                Common.isBluetooth = true;
            }
            else
                Toast.makeText(this, "Activez le bluetooth pour le guidage des servomoteur",Toast.LENGTH_LONG).show();
        }
    }
}
