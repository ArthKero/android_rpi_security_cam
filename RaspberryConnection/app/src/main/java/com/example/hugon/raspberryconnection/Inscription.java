package com.example.hugon.raspberryconnection;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by hugon on 07/11/2017.
 */

public class Inscription extends AppCompatActivity {

    EditText email , password;
    Button envoyer;

    private FirebaseAuth auth;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        auth = FirebaseAuth.getInstance();


        email =(EditText)findViewById(R.id.inscription_email);
        password =(EditText)findViewById(R.id.inscription_password);
        envoyer =(Button)findViewById(R.id.inscription_envoyer);
    }

    public void submit(View view){

        String emailText = email.getText().toString().trim();
        String passwordText = password.getText().toString().trim();

        if(TextUtils.isEmpty(emailText)) {
            Toast.makeText(getBaseContext(), "Email empty :)", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passwordText)) {
            Toast.makeText(getBaseContext(), "Password empty :)", Toast.LENGTH_SHORT).show();
            return;
        }

        //email and password not empty , insert into firebase and local bdd!!

        //firebase
        auth.createUserWithEmailAndPassword(emailText,passwordText)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){

                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if(task.isSuccessful()){
                                    Toast.makeText(getApplicationContext(),"Un email vous sera envoyer",Toast.LENGTH_LONG).show();
                                    //passage a l'autre activité une fois le user crée
                                    Intent intent = new Intent(getBaseContext(),Login.class);
                                    startActivity(intent);
                                } else
                                    Toast.makeText(getApplicationContext(),"Erreur lors de la creation",Toast.LENGTH_SHORT).show();

                            }
                        }
                );


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
