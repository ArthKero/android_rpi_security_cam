package com.example.hugon.raspberryconnection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by hugon on 17/10/2017.
 */

public class MyViewHolder extends RecyclerView.ViewHolder{
    private ImageView photo;
    private TextView name;
    private TextView number;
    private Context context;

    //itemView est la vue correspondante à 1 cellule
    public MyViewHolder(View itemView) {
        super(itemView);
        //c'est ici que l'on fait nos findView

        number  = (TextView) itemView.findViewById(R.id.item_name);
        name    = (TextView) itemView.findViewById(R.id.item_date);
        photo   = (ImageView) itemView.findViewById(R.id.item_image);

        context = itemView.getContext();
    }

    //puis ajouter une fonction pour remplir la cellule en fonction d'un MyObject

    public void bind(RaspNotif myObject){
        name.setText(myObject.getNom());

        number.setText("Viewed :" + myObject.getDate());

        if(myObject.getUrl()!= null)
            Glide.with(context)
                .load(myObject.getUrl())
                .into(photo);

    }
}
