package com.example.hugon.raspberryconnection;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class Login extends AppCompatActivity {

    EditText email , password;
    Button envoyer;


    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        auth = FirebaseAuth.getInstance();

        //si deja connecter pas la peine de se logger
        if(auth.getCurrentUser() != null){
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }


        email =(EditText)findViewById(R.id.email);
        password =(EditText)findViewById(R.id.password);
        envoyer =(Button)findViewById(R.id.save);

        //Support du boutton retour
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ic_drawer);
        }

    }

    public void submit(View view){

        final String emailText = email.getText().toString().trim();
        final String passwordText = password.getText().toString().trim();

        if(TextUtils.isEmpty(emailText)) {
            Toast.makeText(Login.this, "Email empty :)", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passwordText)) {
            Toast.makeText(Login.this, "Password empty :)", Toast.LENGTH_SHORT).show();
            return;
        }

        //email and password not empty , insert into firebase and local bdd!!



        //firebase
        auth.signInWithEmailAndPassword(emailText,passwordText)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){

                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if(task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), "Conexion confim :)", Toast.LENGTH_LONG).show();

                                    //passage a l'autre activité une fois le user crée
                                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                                else
                                    Toast.makeText(getApplicationContext(),"Conexion refused :(",Toast.LENGTH_SHORT).show();

                            }
                        }
                );


    }

    public void register (View view){

        Intent intent = new Intent(this,Inscription.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
